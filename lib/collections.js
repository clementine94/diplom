Indicators = new Mongo.Collection('indicators');
Plant = new Mongo.Collection('plant');

var info = {
    id: 1,
    plant_name: "Красная герань",
    description: "Поливать цветок необходимо регулярно и обильно. Помните: герань не переносит, если в вазоне или горшке застаивается вода, поэтому нужно просто поддерживать землю во влажном состоянии, а так же обеспечить хороший дренаж.",
    plot_size: 25,
    koeff: 7
};

Plant.insert(info);