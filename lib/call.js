Call = function () {
    var args = Array.prototype.slice.call(arguments);

    return new Promise(function (resolve, reject) {
        args.push(function(error, result){
            if (error) {
                reject(error);
                return;
            }
            resolve(result);
        });

        Meteor.call.apply(Meteor, args);
    });
};
