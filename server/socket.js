var socket = io('http://localhost:3000');

function isNumber(val) {
    return Number(val) == val;
}

socket.on('poliv', Meteor.bindEnvironment(function poliv(msg) {

    if (typeof msg == 'string') {
        try {
            JSON.parse(msg);
        }

        catch (e) {
            return ;
        }
    }

    var watering = msg && msg.water || false;
    
    if (msg && isNumber(msg.temperature) && isNumber(msg.wet)) {
        Indicators.insert({
            date: new Date().getTime(),
            temperature: msg.temperature,
            wet: msg.wet,
            watering: watering
        });
    }
    console.log(msg);
}));

Meteor.methods({
    poliv: function() {
        socket.emit('doit', true);
        console.log('emit');
    }
});
