Meteor.methods({
    getIndicator: function (selector, field) {
        var options = {
            sort: {date: 1},
            fields: {date: 1}
        };
        options.fields[field] = 1;

        return Indicators.find(selector, options).map(function (doc) {
            return [doc.date, doc[field]];
        });
    },

    getDateWatering: function() {
        var options = {
            sort: {date: 1}

        };

        var cursor = Indicators.find({watering: true}, options);

        return cursor.map(function(elem) {
            return {
                x: elem.date,
                title: "П",
                text: "<strong>Температура</strong> " + elem.temperature + " <br><strong>Влажность почвы</strong> " + elem.wet
            };
        });
    }
});
