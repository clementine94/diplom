Meteor.startup(function () {
    var count = Indicators.find().count();

    if (count === 0) {
        var date = new Date().getTime();
        for (var i = 0; i < 500; i++) {
            date -= 3 * 60 * 60 * 1000;
            var temp = {
                date: date,
                temperature: Math.round(Math.random() * 45 - 15),
                wet: Math.round(Math.random() * 50 + 50)
            };

            if (Math.random() > 0.85) {
                temp.watering = true;
            }

            Indicators.insert(temp);
        }
    }

    var api = new CollectionAPI({
        apiPath: "api"
    });

    api.addCollection(Indicators, 'indicators', {
        methods: ['POST']
    });

    api.start();
});
