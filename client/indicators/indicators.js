//Template.today.helpers({
//    time: function(){
//        var date = new Date();
//        return "" + date.toLocaleDateString();
//    }
//});

Template.last.helpers({
    time: function(){
        var data = Indicators.findOne({}, {
            sort: {date: -1}
        });

        if (data && data.date) {
            return new Date(data.date).toLocaleDateString();
        }

        return 'N/A';
    }
});

Template.temperature.helpers({
    t: function () {
        var data = Indicators.findOne({}, {
            sort: {date: -1}
        });

        if (data && data.temperature) {
            return data.temperature;
        }

        return 'N/A';
    }
});

Template.wet.helpers({
    w: function(){
        var data = Indicators.findOne({}, {
            sort: {date: -1}
        });

        if (data && data.wet) {
            return data.wet;
        }

        return 'N/A';
    }
});
