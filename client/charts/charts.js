Template.chart.onRendered(function () {
    var promises = [
        Call('getIndicator', {}, 'temperature'),
        Call('getIndicator', {}, 'wet'),
        Call('getDateWatering')
    ];

    Promise.all(promises).then(function (data) {

        Highcharts.setOptions({
            lang: {
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',  'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                shortMonths: ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня',  'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
                rangeSelectorZoom: 'Период '
            }
        });
        // Create the chart
        var $container = $('#container');
        $container.highcharts('StockChart', {


            rangeSelector: {
                selected: 0,
                buttons: [{
                    type: 'day',
                    count: 1,
                    text: 'День'
                },{
                    type: 'day',
                    count: 3,
                    text: '3 Дня'
                }, {
                    type: 'week',
                    count: 1,
                    text: '1 Неделя'
                }, {
                    type: 'week',
                    count: 2,
                    text: '2 Недели'
                }, {
                    type: 'all',
                    text: 'Все время'
                }],
                inputEnabled: false,
                buttonTheme: {
                    fill: 'none',
                    width: 70,
                    r: 3,
                    style: {
                        color: '#3f51b5',
                        fontWeight: 'lighter'
                    },
                    states: {
                        hover: {},
                        select: {
                            fill: '#3f51b5',
                            style: {
                                color: 'white',
                                fontWeight: 'lighter'
                            }
                        }
                    }
                },
                labelStyle: {
                    color: 'black',
                    fontWeight: 'bold'
                }
            },

            title: {
                text: 'График полива растений'
            },

            tooltip: {
                style: {
                    width: '200px'
                },
                valueDecimals: 4,
                shared: true
            },

            yAxis: {
                title: {
                    text: 'Показатели'
                }
            },
            legend: {
                enabled: true
            },
            series: [{
                id: 'temperature',
                name: 'Температура',
                data: data[0],
                marker : {
                    enabled : true,
                    radius : 3
                },
                shadow : true
            }, {
                type: 'flags',
                name: 'Пометки полива',
                data: data[2],
                onSeries: 'temperature',
                shape: 'circlepin',
                width: 16
            }]
        });

        $container.data('indicator', 'temperature');

        var observerOfIndicators = Indicators.find({}).observe({
            added: function(){
                if(!observerOfIndicators) return;

                var indicator = $container.data('indicator');
                updateIndicators(indicator);
            }
        });
    });
});

Template.chart.events({
    'change [name="graphics"]': function(event) {
        var indicator = $(event.target).val();
        updateIndicators(indicator);
    }
});

function updateIndicators(indicator) {
    var promises = [
      Call('getIndicator', {}, indicator),
      Call('getDateWatering')
    ];
    Promise.all(promises).then(function (data) {
        var name = indicator == "wet" ? "Влажность" : "Температура";
        var $container = $('#container');
        var chart = $container.highcharts();
        chart.series[0].update({
            name: name,
            data: data[0]
        });
        chart.series[1].update({
            data: data[1]
        });
        $container.data('indicator', indicator);
    });
}

